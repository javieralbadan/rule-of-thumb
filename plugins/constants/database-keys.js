export const DATABASE_KEYS = {
  PERSONS: 'persons',
  VOTES: 'votes',
  POSITIVE: 'positive',
  NEGATIVE: 'negative',
}
