// @ts-check

import * as firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'

const config = {
  apiKey: 'AIzaSyATqC6cdlfVwbwUlISfij7QjgNBtjfpMe8',
  authDomain: 'rule-of-thumb-test.firebaseapp.com',
  databaseURL: 'https://rule-of-thumb-test.firebaseio.com',
  projectId: 'rule-of-thumb-test',
  storageBucket: 'rule-of-thumb-test.appspot.com',
  messagingSenderId: '908396814806',
}

!firebase.apps.length ? firebase.initializeApp(config) : ''

export const GoogleProvider = new firebase.auth.GoogleAuthProvider()
export const FacebookProvider = new firebase.auth.FacebookAuthProvider()
export const TwitterProvider = new firebase.auth.TwitterAuthProvider()
export const auth = firebase.auth()
export const database = firebase.database()

/**
 * export default firebase
 */
export default ({}, inject) => {
  inject('GoogleProvider', GoogleProvider)
  inject('FacebookProvider', FacebookProvider)
  inject('TwitterProvider', TwitterProvider)
  inject('auth', auth)
  inject('db', database)
}
