export default ({ app, store }) => {
  app.router.beforeEach((to, from, next) => {
    !store.state.persons ? getPersons() : null
    next()
  })
  app.router.afterEach(() => {
    closeModal()
  })

  /**
   * Cargar personas
   */
  function getPersons() {
    const { dispatch } = store
    dispatch('loadPersons')
  }

  /**
   * Cerrar ventana modal, en caso de navegación
   */
  function closeModal() {
    const { dispatch } = store
    const resource = 'modalNav'
    const payload = false
    dispatch('toggleModal', { resource, payload })
  }
}
