import Vue from 'vue'
import VueI18n from 'vue-i18n'

Vue.use(VueI18n)

export default ({ app }) => {
  app.i18n = new VueI18n({
    silentFallbackWarn: true,
    locale: process.env.lang,
    fallbackLocale: 'en-EN',
    messages: {
      'en-EN': require('~/locales/en-EN.json'),
    },
  })
}
