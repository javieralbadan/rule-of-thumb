export default {
  mode: 'spa',
  env: {
    lang: 'en-EN',
  },
  server: {
    port: 4000,
  },
  generate: {
    dir: 'public',
  },
  /*
   ** Headers of the page
   */
  head: {
    htmlAttrs: {
      lang: 'en',
    },
    title: 'Rule of Thumb.',
    meta: [
      { charset: 'utf-8' },
      { 'http-equiv': 'X-UA-Compatible', content: 'IE=edge' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        name: 'keywords',
        content: 'Zemoga, frontend, test, javier albadan',
      },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || '',
      },
      { name: 'author', content: 'Javier Albadán' },
      { name: 'theme-color', content: '#158580' },
      { name: 'msapplication-navbutton-color', content: '#158580' },
      { name: 'apple-mobile-web-app-status-bar-style', content: '#158580' },
    ],
    link: [
      { rel: 'icon', type: 'image/png', href: '/favicon.png' },
      {
        rel: 'stylesheet',
        href: 'https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/components/site.min.css',
      },
      {
        rel: 'stylesheet',
        href: 'https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/components/container.min.css',
      },
      { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/components/icon.min.css' },
    ],
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#158580' },
  /*
   ** Global CSS
   */
  css: ['~assets/scss/main.scss', '~assets/css/vivify.min.css'],
  /*
   ** Router configuration
   */
  router: {
    linkActiveClass: 'active',
  },
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    { src: '~/plugins/i18n.js' },
    { src: '~/plugins/firebase', ssr: false },
    { src: '~/plugins/router-after-each', ssr: false },
    { src: '~/plugins/v-lazy-image', ssr: false },
    { src: '~/plugins/vue-progressive-image', ssr: false },
    { src: '~/plugins/intersection-observer', ssr: false },
    { src: '~/plugins/vue-observe-visibility', ssr: false },
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv',
  ],
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {},
  },
}
