import { DATABASE_KEYS } from '~/plugins/constants/database-keys'
import { database } from '~/plugins/firebase'

export const state = () => ({
  modalNav: false,
  persons: undefined,
})

export const mutations = {
  mutation(state, { resource, payload }) {
    state[resource] = payload
  },
}

export const actions = {
  loadPersons({ commit }) {
    database.ref(DATABASE_KEYS.PERSONS).on('value', (snapshot) => {
      const response = snapshot.val()
      if (response) {
        for (const key in response) {
          response[key].id = key
        }
        commit('mutation', { resource: 'persons', payload: response })
      }
    })
  },
  toggleModal({ commit }, { resource, payload }) {
    commit('mutation', { resource, payload })
  },
}

export const getters = {
  modalNav: (state) => state.modalNav,
  persons: (state) => state.persons,
}
