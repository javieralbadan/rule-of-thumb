import { DATABASE_KEYS } from '~/plugins/constants/database-keys'
import { database } from '~/plugins/firebase'

/**
 * Votar por un personaje, enviando personaje y 'positive' o 'negative'
 * @param {Object} person
 * @param {String} vote
 */
export async function generateVote(person, vote) {
  const updateObject = {}
  const newVote = person[DATABASE_KEYS.VOTES][vote] + 1
  try {
    updateObject[`${DATABASE_KEYS.PERSONS}/${person.id}/${DATABASE_KEYS.VOTES}/${vote}`] = newVote
    console.log('generateVote -> updateObject', `${DATABASE_KEYS.PERSONS}/${person.id}/${DATABASE_KEYS.VOTES}/${vote}`)
    await database.ref().update(updateObject)
    console.log('log ok')
    return { error: null, response: true }
  } catch (error) {
    return { error: true, response: false }
  }
}
