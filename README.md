# Rule of Thumb.

> Test Zemoga Frontend. Applicant: Javier Albadán (javier.albadan@gmail.com).

You can see app [here](https://rule-of-thumb-test.web.app/). This test was developed with the following technical specifications:

### Front

- [x] Nuxt.js
- [x] Vue.js
- [x] Sass
- [x] Semantic UI (a couple modules)

### Back

- [x] Firebase Realtime Database
- [x] Firebase Storage
- [x] Firebase Hosting

### Testing details

- [x] Testing framework jest
- [x] Unit testing in `CardActions.spec.js` and `CardResultsBar.spec.js` only
- [x] Tests files were created in `test` folder for ease

#### Technical approach

Development process was made with the following considerations:

- [x] Site reactivity through realtime database
- [x] Language features worked with `i18n` module
- [x] Datetime format worked with `moment.js`
- [x] Orientation to Clean Code
- [x] Semantic HTML5 and CSS3 features were implemented
- [x] Use of BEM
- [x] CSS animation library: `vivify` ()
- [x] Progressive image loading
- [x] Images were compressed before they upload to server
- [x] Eslint and Prettier were used
- [x] CVS: Two branches, `master` for entire project, and ohter one for `firebase-integration`

## ToDo

Below the aspects not implement

- [] Authentication module, that was thought to be done with Firebase Auth
- [] Improved accuracy of using color with transparencies
- [] Work in dynamic content loading and voting interaction in main box
- [] Dynamic load of the closing date of the main votation

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
