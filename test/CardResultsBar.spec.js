import { shallowMount } from '@vue/test-utils'
import CardResultsBar from '~/components/cards/CardResultsBar.vue'

const propsData = {
  votes: {
    positive: 8,
    negative: 2,
  },
}
const stubs = {}
const factory = (props) => {
  return shallowMount(CardResultsBar, {
    propsData: props,
    stubs: stubs,
    mocks: {
      $events: {
        loadAnimation: () => {},
      },
      $options: {
        filters: {
          percentage: () => {},
        },
      },
      watcher: {
        positiveAverage: () => {},
        negativeAverage: () => {},
      },
    },
  })
}

describe('CardResultsBar', () => {
  test('Shoulds generate initial values correctly', () => {
    const wrapper = factory(propsData)
    expect(wrapper.vm.votesUp).toEqual(propsData.votes.positive)
    expect(wrapper.vm.votesDown).toEqual(propsData.votes.negative)
    expect(wrapper.vm.totalVotes).toEqual(propsData.votes.positive + propsData.votes.negative)
  })

  test('Shoulds get average as corresponding', () => {
    const wrapper = factory(propsData)
    expect(wrapper.vm.positiveAverage).toEqual(80)
    expect(wrapper.vm.negativeAverage).toEqual(20)
  })

  test('loadAnimation is called a twice by the both changes of averages after update votes', async () => {
    const wrapper = factory(propsData)
    const spyVoteNow = jest.spyOn(wrapper.vm, 'loadAnimation')
    wrapper.setProps({
      votes: {
        positive: 8,
        negative: 3,
      },
    })
    await wrapper.vm.$nextTick()
    expect(spyVoteNow).toHaveBeenCalledTimes(2)
  })

  test('Shoulds get average as corresponding', () => {
    const wrapper = factory(propsData)
    expect(wrapper.vm.positiveAverage).toEqual(80)
    expect(wrapper.vm.negativeAverage).toEqual(20)
  })
})
