import { shallowMount } from '@vue/test-utils'
import { DATABASE_KEYS } from '~/plugins/constants/database-keys'
import CardActions from '~/components/cards/CardActions.vue'

const propsData = {
  voted: true,
}
const stubs = {}
const factory = (props) => {
  return shallowMount(CardActions, {
    propsData: props,
    stubs: stubs,
    mocks: {
      $events: {
        voteNow: () => {},
        voteAgain: () => {},
      },
    },
  })
}

describe('CardActions component before has voted', () => {
  test('Shoulds render "options" at the beginning and no "vote-again"', () => {
    const wrapper = factory({})
    expect(wrapper.find('.vote-card__options').exists()).toBeTruthy()
    expect(wrapper.find('.vote-card__vote-again').exists()).toBeFalsy()
  })

  test('Selection attribute was assigned when is clicked .btn--up', () => {
    const wrapper = factory({})
    wrapper.find('.btn--up').trigger('click')
    expect(wrapper.vm.selection).toEqual(DATABASE_KEYS.POSITIVE)
  })

  test('Selection attribute was assigned when is clicked .btn--down', () => {
    const wrapper = factory({})
    wrapper.find('.btn--down').trigger('click')
    expect(wrapper.vm.selection).toEqual(DATABASE_KEYS.NEGATIVE)
  })

  test('voteNow method can not called when "vote-now" is clicked, because is disabled', async () => {
    const wrapper = factory({})
    const spyVoteNow = jest.spyOn(wrapper.vm, 'voteNow')
    wrapper.find('.vote-card__vote-now').trigger('click')
    expect(spyVoteNow).toHaveBeenCalledTimes(0)
  })

  test('voteNow method is called when "vote-now" is clicked, after selection change', async () => {
    const wrapper = factory({})
    const spyVoteNow = jest.spyOn(wrapper.vm, 'voteNow')
    wrapper.vm.selection = DATABASE_KEYS.POSITIVE
    await wrapper.vm.$nextTick()
    wrapper.find('.vote-card__vote-now').trigger('click')
    expect(spyVoteNow).toHaveBeenCalledTimes(1)
  })

  test('voteNow method call emit event whit selection', async () => {
    const wrapper = factory({})
    const spyEmit = jest.spyOn(wrapper.vm, '$emit')
    wrapper.vm.selection = DATABASE_KEYS.POSITIVE
    wrapper.vm.voteNow()
    await wrapper.vm.$nextTick()
    expect(spyEmit).toHaveBeenCalledTimes(1)
    expect(spyEmit).toHaveBeenCalledWith('vote', DATABASE_KEYS.POSITIVE)
  })
})

describe('CardActions component after has voted', () => {
  test('When voted prop comes in true, shoulds render "vote-again" and no "vote-again"', async () => {
    const wrapper = factory(propsData)
    expect(wrapper.find('.vote-card__options').exists()).toBeFalsy()
    expect(wrapper.find('.vote-card__vote-again').exists()).toBeTruthy()
  })

  test('voteAgain method is called when "again" is clicked, after selection change', async () => {
    const wrapper = factory(propsData)
    const spyVoteAgain = jest.spyOn(wrapper.vm, 'voteAgain')
    await wrapper.vm.$nextTick()
    wrapper.find('.vote-card__vote-again').trigger('click')
    expect(spyVoteAgain).toHaveBeenCalledTimes(1)
  })

  test('voteAgain method call emit event', async () => {
    const wrapper = factory(propsData)
    const spyEmit = jest.spyOn(wrapper.vm, '$emit')
    wrapper.vm.voteAgain()
    await wrapper.vm.$nextTick()
    expect(spyEmit).toHaveBeenCalledTimes(1)
    expect(spyEmit).toHaveBeenCalledWith('vote-again')
  })
})
